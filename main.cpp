#ifdef _WIN32
#include <Windows.h>
#endif
#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/glut.h>
#include "engine/includes/GameClient.hpp"
using namespace engine;

int main(int argc, char *argv[]) {
  GameClient * gclient = new GameClient();
  ProcessInfo * pProcessInfo = new ProcessInfo();

  pProcessInfo->argc  = argc;
  pProcessInfo->argv  = argv;
  pProcessInfo->title = "Simple Glut Window";

  gclient->init(pProcessInfo);

  gclient->run();

  delete gclient;
  delete pProcessInfo;
  return 0;
}
