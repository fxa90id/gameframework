#include <GL/gl.h>
#include <glm/glm.hpp>
#include <iostream>

#include "engine/Particle.hpp"

using namespace Engine::Model;
using namespace std;

Particle::Particle() {
  static long gid = 0;
  id = gid++;
  textureHandle = 0;
  vboHandle = 0;
  shaderHandle = 0;
  verticesCount = 0;
  ttl = 0;
  position = glm::vec3(0, 0, 0);
}

Particle::~Particle() {
}

void Particle::bindColors(float red, float green, float blue) {
  color[COLOR_RED]    = red;
  color[COLOR_GREEN]  = green;
  color[COLOR_BLUE]   = blue;
}

void Particle::bindTexture(unsigned int textureIdent) {
  textureHandle = textureIdent;
}

void Particle::bindShader(unsigned int shaderIdent) {
  shaderHandle = shaderIdent;
}

void Particle::draw() {
/*  glUseProgram(shaderHandle);
  glBindTexture(GL_TEXTURE_2D, textureHandle);
  glTranslatef(position.x, position.y, position.z);
  glDrawArrays(GL_TRIANGLES, 0, verticesCount); */
 glTranslatef(position.x, position.y, position.z);
  glColor3fv(color);
  glBegin(GL_QUADS);
    glVertex3f(-0.01, -0.01, 0);
    glVertex3f(-0.01,  0.01, 0);
    glVertex3f( 0.01,  0.01, 0);
    glVertex3f( 0.01, -0.01, 0);
  glEnd();
  glFlush();
/*  cout << "particle(" << id << "): [";
  cout << position.x << ", ";
  cout << position.y << ", ";
  cout << position.z << "]" << endl; */
}

void Particle::setPosition(const glm::vec3& pos) {
  position = pos;
}

void Particle::move(const float x, const float y, const float z) {
  position.x += x;
  position.y += y;
  position.z += z;
}
