#include <GL/glew.h>
#include <GL/gl.h>
#include <GL/glu.h>
#include <GL/freeglut.h>

#include <iostream>

#include <engine/System.hpp>
#include <Interface/Visual.hpp>
#include <engine/VisualLinux.hpp>

using namespace engine::visual;

VisualLinux::VisualLinux() {
}

VisualLinux::~VisualLinux() {
  shutdown();
}

bool VisualLinux::init(engine::System * pSystem) {
  engine::System * pSys = pSystem;
  glutInit(&(pSys->getProcessInfo()->argc), pSys->getProcessInfo()->argv);
  glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
  glutInitContextVersion(3, 0);
  glutInitContextProfile(GLUT_CORE_PROFILE);
  glutInitWindowSize(
    pSys->getWindow()->width,
    pSys->getWindow()->height
  );
  glutInitWindowPosition(
    pSys->getWindow()->startPosX,
    pSys->getWindow()->startPosY
  );
  glutCreateWindow(pSys->getProcessInfo()->title);
  GLenum res = glewInit();
  if(res != GLEW_OK) {
    std::cout << glewGetErrorString(res) << std::endl;
    exit(1);
  }
  glutDisplayFunc(VisualLinux::Render);
  return true;
}

void VisualLinux::shutdown() {
}

void VisualLinux::beginRender() {
  glClearColor(0.2, 0.2, 0.2, 1.0);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glLoadIdentity();
}

void VisualLinux::endRender() {
  glutSwapBuffers();
  glutPostRedisplay();
}
void VisualLinux::loop() {
  glutMainLoop();
}
void VisualLinux::Render() {
  me.beginRender();
  me.endRender();
}
