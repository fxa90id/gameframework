#include "includes/System.hpp"
#include "includes/ProcessInfo.hpp"

using namespace engine;

System::System() {
  m_Window      = nullptr;
  m_Visual      = nullptr;
  m_Logger      = nullptr;
  m_ProcessInfo = nullptr;
}

System::~System() {
 shutdown();
}

void System::shutdown() {
   if(m_Window) {
    delete m_Window;
  }
  if(m_Visual) {
    delete m_Visual;
  }
  if(m_Logger) {
    delete m_Logger;
  }
  if(m_ProcessInfo) {
    delete m_ProcessInfo;
  }
}

Window * System::getWindow() {
  return m_Window;
}

void System::setWindow(Window * pWindow) {
  m_Window = pWindow;
}

Visual* System::getVisual() {
  return m_Visual;
}

void System::setVisual(Visual * pVisual) {
  m_Visual = pVisual;
}

Logger * System::getLogger() {
  return m_Logger;
}

void System::setLogger(Logger * pLogger) {
  m_Logger = pLogger;
}

ProcessInfo * System::getProcessInfo() {
  return m_ProcessInfo;
}

void System::setProcessInfo(ProcessInfo * pProcessInfo) {
  m_ProcessInfo = pProcessInfo;
}
