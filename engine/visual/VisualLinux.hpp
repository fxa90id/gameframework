#ifndef __VisualLinux_hpp_INCLUDE__
#define __VisualLinux_hpp_INCLUDE__

#include <System.hpp>
#include <Interface/Visual.hpp>


namespace engine {
  namespace visual {
    class VisualLinux : public engine::interface::Visual {
    public:
      VisualLinux();
      virtual ~VisualLinux();
      virtual bool init(System * pSystem);
      virtual void shutdown();
      virtual void beginRender();
      virtual void endRender();
      virtual void loop();
    protected:
      static void Render();
    };
  };
};
#endif
