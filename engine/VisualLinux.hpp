#ifndef __VisualLinux_hpp_INCLUDE__
#define __VisualLinux_hpp_INCLUDE__
namespace engine {
  class System;
  class interface::Visual;
  namespace visual {
    class VisualLinux : public engine::interface::Visual {
    public:
      VisualLinux();
      virtual ~VisualLinux();
      virtual bool init(engine::System * pSystem);
      virtual void shutdown();
      virtual void beginRender();
      virtual void endRender();
      virtual void loop();
    protected:
      static void Render();
      static VisaulLinux me;
    };
  };
};
#endif
