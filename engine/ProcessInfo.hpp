#ifndef __ProcessInfo_hpp_INCLUDE__
#define __ProcessInfo_hpp_INCLUDE__
namespace engine {
  class ProcessInfo {
  public:
    ProcessInfo() {};
    ~ProcessInfo() {};
    int argc;
    char ** argv;
    char * title;
  };
};
#endif
