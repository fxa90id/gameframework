#ifndef __Visual_hpp_INCLUDE__
#define __Visual_hpp_INCLUDE__

namespace engine {
  namespace interface {
    class System;
    class Visual {
    public:
      Visual() {};
      virtual ~Visual() {};
      virtual bool init(System *) = 0;
      virtual void shutdown() = 0;
      virtual void beginRender() = 0;
      virtual void endRender() = 0;
      virtual void loop() = 0;
    };
  };
};

#endif