#ifndef _Mesh_hpp_INCLUDE__
#define _Mesh_hpp_INCLUDE__

#include <glm/glm.hpp>

namespace engine {
  namespace interface {
    class Mesh {
    public:
      Mesh() {};
      virtual ~Mesh() {};
      virtual void draw() = 0;
      virtual void bindColors(float, float, float) = 0;
      virtual void bindTexture(unsigned int) = 0;
      virtual void bindShader(unsigned int) = 0;
      virtual void setPosition(const glm::vec3&) = 0;
    };
  };
};
#endif