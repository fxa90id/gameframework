#ifndef __Window_hpp_INCLUDE__
#define __Window_hpp_INCLUDE__
namespace engine {
  struct Window {
    Window() {
      width = 640;
      height = 480;
      startPosX = 100;
      startPosY = 100;
    }
    unsigned int width;
    unsigned int height;
    unsigned int startPosX;
    unsigned int startPosY;
  };
};
#endif