#ifndef __Logger_hpp_INCLUDE__
#define __Logger_hpp_INCLUDE__

#include <string>
#include <fstream>

namespace engine {
  class Logger {
    private:
      std::fstream log;
    public:
      Logger(const std::string&);
      ~Logger();
      
      Logger& operator<<(const char *);
      Logger& operator<<(const std::string&);

    private:
      void open(const std::string&);
      void close();
  }; // class Logger
}; // namespace engine
 #endif