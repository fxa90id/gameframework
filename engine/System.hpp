#ifndef __System_hpp_INCLUDE__
#define __System_hpp_INCLUDE__

#include <engine/Window.hpp>
#include <engine/Logger.hpp>
#include <engine/ProcessInfo.hpp>
#include <engine/interface/Visual.hpp>

namespace engine {
  class System {
  public:
    System();
    ~System();
    // getery
    engine::Window * getWindow();
    engine::interface::Visual * getVisual();
    engine::Logger * getLogger();
    engine::ProcessInfo * getProcessInfo();
    // setery
    void setVisual(engine::interface::Visual * pVisual);
    void setLogger(engine::Logger * pLogger);
    void setWindow(engine::Window * pWindow);
    void setProcessInfo(engine::ProcessInfo * pProcessInfo);
    void shutdown();
  protected:
    engine::Window * m_Window;
    engine::Logger * m_Logger;
    engine::ProcessInfo * m_ProcessInfo;
    engine::interface::Visual * m_Visual;
  };
};
#endif
