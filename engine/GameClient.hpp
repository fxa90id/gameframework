#ifndef __GameClient_hpp_INCLUDE__
#define __GameClient_hpp_INCLUDE__

#include <enigne/System.hpp>

namespace engine {
  class engine::ProcessInfo;
  class engine::controller::System;
  class GameClient {
  public:
    GameClient();
    ~GameClient();
    bool init(engine::ProcessInfo *);
    void shutdown();
    void run();
  protected:
    engine::controller::System * m_System;
  };
};
#endif
