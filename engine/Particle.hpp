#ifndef __Particle_hpp_INCLUDE__
#define __Particle_hpp_INCLUDE__

#include <Interface/Mesh.hpp>

namespace Engine {
  namespace Model {
    class Particle : public Engine::Interface::Mesh {
      public:
        Particle();
        virtual ~Particle();
        virtual void draw();
        virtual void bindColors(float, float, float);
        virtual void bindTexture(unsigned int);
        virtual void bindShader(unsigned int);
        virtual void setPosition(const glm::vec3&);
        virtual void move(const float, const float, const float);
        virtual void setTTL(unsigned long t) {
          ttl = t;
        }
        virtual unsigned long getTTL() {
          return ttl;
        }
      protected:
        float color[3];
        float angle;
        long id;
        unsigned int vboHandle;
        unsigned int verticesCount;
        unsigned int textureHandle;
        unsigned int shaderHandle;
        unsigned long ttl; // time to live
        glm::vec3 position;
      private:
        enum {
          COLOR_RED = 0,
          COLOR_GREEN,
          COLOR_BLUE
        };
    };
  };
};
#endif