#include <iostream>
#include <exception>
#include <fstream>
#include <string>

#include "includes/Logger.hpp"

using namespace engine;
using namespace std;

Logger::Logger(const string& filepath) {
  open(filepath);
}

Logger::~Logger() {
  close();
}

Logger& Logger::operator<<(const string& message) {
  log << message << endl;
  return *this;
}

Logger& Logger::operator<<(const char * message) {
  log << message << endl;
  return *this;
}

void Logger::open(const string& filepath) {
  log.open(filepath.c_str(), std::ios::out | std::ios::ate);
  if(!log.is_open()) {
    if(!log.good()) {
      throw new exception;
    }
  }
}

void Logger::close() {
  if(log.is_open()) {
    log.close();
  }
}