#include "includes/GameClient.hpp"

using namespace engine;

bool GameClient::init(ProcessInfo * pProcessInfo) {
  m_System = new System();

  m_System->setProcessInfo(pProcessInfo);

  m_System->setLogger(new Logger("log.txt"));
  
  Window * window     = new Window();

  m_System->setWindow(window);

  Visual * visual = new VisualLinux();

  visual->init(m_System);

  m_System->setVisual(visual);

  return true;
}

void GameClient::shutdown() {
  if(m_System) {
    delete m_System;
  }
}

void GameClient::run() {
 m_System->getVisual()->loop();
}
